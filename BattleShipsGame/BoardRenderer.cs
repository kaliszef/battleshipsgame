﻿using BattleShipGameLogic;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleShipsGame
{
    public class BoardRenderer
    {
        public const char missSign = ' ';
        public const char emptySign = '?';
        public const char hitSign = 'o';
        public const char destroyedSign = 'X';

        private Board board;
        private char[,] gridState;
        private IList<string> gridRowsToRender = new List<string>();

        public Board Board => board;

        public BoardRenderer(Board enemyBoard)
        {
            try
            {
                board = enemyBoard;
                gridState = new char[board.ColumnsCount, board.RowsCount];
                CreateInitialGridState();
            }
            catch(Exception ex)
            {
                if(ex is OutOfMemoryException || ex is IndexOutOfRangeException)
                {
                    throw new BoardRendererException("Board size is to big and cannot be rendered. Try to build smaller board.");
                }
                throw;
            }
        }

        public void Shoot(string target)
        {
            var shotResult = board.Shoot(target);
            var charResult = PickResultChar(shotResult);

            if (shotResult == ShotReport.Sinks)
                MarkDestroyedShip(target, charResult);
            else
                MarkShot(target, charResult);
        }

        public void RenderGridState()
        {

            BuildGrid();

            foreach(var line in gridRowsToRender)
            {
                Console.WriteLine(line);
            }

            gridRowsToRender.Clear();
        }

        private char PickResultChar(ShotReport shotResult)
        {
            switch (shotResult)
            {
                case ShotReport.Hit:
                    return hitSign;
                case ShotReport.Sinks:
                    return destroyedSign;
                case ShotReport.Miss:
                    return missSign;
                default:
                    return emptySign;
            }
        }

        private void MarkShot(string target, char shotResult)
        {
            var targetCoordinates = new Coordinates(target);

            int targetColumnIndex = board.GetColumnIndex(targetCoordinates.Column);
            int targetRowIndex = board.GetRowIndex(targetCoordinates.Row);

            gridState[targetRowIndex, targetColumnIndex] = shotResult;
        }

        private void MarkDestroyedShip(string target, char shotResult)
        {
            IEnumerable<Coordinates> shipCoordinates = board.GetDestroyedShipSegments(target);

            foreach(var shipSegment in shipCoordinates)
            {
                int targetColumnIndex = board.GetColumnIndex(shipSegment.Column);
                int targetRowIndex = board.GetRowIndex(shipSegment.Row);

                gridState[targetRowIndex, targetColumnIndex] = shotResult;
            }
        }

        private void BuildGrid()
        {
            CreateHeader();
            CreateGridRows();
        }

        private void CreateHeader()
        {
            var headerBuilder = new StringBuilder();

            headerBuilder.AppendFormat(" {0,4}", " ");
            headerBuilder.AppendJoin<char>(" ", board.ColumnsRange);

            gridRowsToRender.Add(headerBuilder.ToString());
        }

        private void CreateGridRows()
        {
            for (int i = 0; i < board.RowsCount; i++)
            {
                StringBuilder rowBuilder = new StringBuilder();
                rowBuilder.Append(" ");
                rowBuilder.AppendFormat("{0,3}", board.RowsRange[i]);
                rowBuilder.Append(" ");
                rowBuilder.AppendJoin(" ", gridState.GetRow(i));

                gridRowsToRender.Add(rowBuilder.ToString());
            }
        }

        private void CreateInitialGridState()
        {
            int rowsCount = gridState.RowsCount();
            int columnsCount = gridState.ColumnsCount();

            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    gridState[i, j] = emptySign;
                }
            }
        }

    }
}
