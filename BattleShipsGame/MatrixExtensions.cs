﻿using System.Linq;

namespace BattleShipsGame
{
    public static class MatrixExtensions
    {
        public static T[] GetRow<T>(this T[,] matrix, int rowNumber)
        {
            return Enumerable.Range(0, matrix.GetLength(1))
                    .Select(x => matrix[rowNumber, x])
                    .ToArray();
        }

        public static int RowsCount<T>(this T[,] matrix)
        {
            return matrix.GetLength(0);
        }

        public static int ColumnsCount<T>(this T[,] matrix)
        {
            return matrix.GetLength(1);
        }
    }
}
