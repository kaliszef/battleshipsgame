﻿using BattleShipGameLogic;
using System;

namespace BattleShipsGame
{
    class Program
    {
        private static ConsoleKey exit = default(ConsoleKey);

        static void Main(string[] args)
        {

            Board computerBoard;
            BoardRenderer computerBoardRenderer;

            while (!(exit == ConsoleKey.Escape))
            {
                try
                {
                    computerBoard = BoardBuilder.CreateBoardWithShips();
                    computerBoardRenderer = new BoardRenderer(computerBoard);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return;
                }

                GameplayLoop(computerBoard, computerBoardRenderer);
            }

        }

        private static void GameplayLoop(Board computerBoard, BoardRenderer computerBoardRenderer)
        {
            string lastExceptionMessage = string.Empty;

            while (!computerBoard.AllShipsDestroyed())
            {
                try
                {
                    Console.Clear();
                    RenderExceptionIfAny(lastExceptionMessage);

                    Console.WriteLine("Computer Grid");
                    computerBoardRenderer.RenderGridState();

                    Console.WriteLine(Environment.NewLine);
                    Console.WriteLine("Enter coordinates: ");

                    string playerShot = Console.ReadLine();
                    computerBoardRenderer.Shoot(playerShot);
                    Console.Clear();

                    computerBoardRenderer.RenderGridState();

                    lastExceptionMessage = string.Empty;
                }
                catch (ArgumentException ex)
                {
                    lastExceptionMessage = $"Exception: {ex.Message}";
                }
            }
            RenderWon();
            PlayAgainOrEndGame();
        }

        private static void PlayAgainOrEndGame()
        {
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("Press 'ESC' to Exit. Any other key will start new game.");
            exit = Console.ReadKey(true).Key;
        }

        private static void RenderExceptionIfAny(string lastExceptionMessage)
        {
            if(!string.IsNullOrWhiteSpace(lastExceptionMessage))
            {
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine(lastExceptionMessage);
                Console.WriteLine(Environment.NewLine);
            }
        }

        private static void RenderWon()
        {
            Console.WriteLine(@"__     __           __          __             _ ");
            Console.WriteLine(@"\ \   / /           \ \        / /            | |");
            Console.WriteLine(@" \ \_/ /__   _   _   \ \  /\  / / __   _ __   | |");
            Console.WriteLine(@"  \   / _ \ | | | |   \ \/  \/ / _  \ | '_ \  | |");
            Console.WriteLine(@"   | | (_) || |_| |    \  /\  / (_) | | | | | |_|");
            Console.WriteLine(@"   |_|\___/ \__,_ |     \/  \/ \___ / |_| |_| (_)");
        }
    }
}
