﻿using System;

namespace BattleShipsGame
{
    [Serializable]
    internal class BoardRendererException : Exception
    {
        public BoardRendererException(string message) : base(message)
        {
        }
    }
}