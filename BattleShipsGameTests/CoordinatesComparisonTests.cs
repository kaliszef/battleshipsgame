﻿using BattleShipGameLogic;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class CoordinatesComparisonTests
    {
        [Fact]
        public void ComparingCoordinatesTypeWithOtherTypeShouldFail()
        {
            Coordinates coordinate = new Coordinates("A1");
            string coordinateString = "test";

            Assert.False(coordinate.Equals(coordinateString));
        }

        [Fact]
        public void ComparingCoordinatesTypeWitNullShouldFail()
        {
            Coordinates coordinate = new Coordinates("A1");

            Assert.False(coordinate.Equals(null));
        }
    }
}
