﻿using BattleShipGameLogic;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class ShootingToShipTests
    {
        [Theory]
        [InlineData("A3")]
        [InlineData("G3")]
        [InlineData("B5")]
        [InlineData("C5")]
        public void WhenShootingToShipAlignHorizontallyAndMissThenShipShouldNotBeHit(string enemyShoot)
        {
            var ship = new Ship(5, new Coordinates("B3"), Alignment.Horizontal);

            bool result = ship.GotHit(enemyShoot);

            Assert.False(result);
        }

        [Theory]
        [InlineData("B3")]
        [InlineData("C3")]
        [InlineData("D3")]
        [InlineData("E3")]
        [InlineData("F3")]
        public void WhenShootingToOneSegmentOfShipAlignVerticallyThenShipShouldBeHit(string enemyShoot)
        {
            var ship = new Ship(5, new Coordinates(enemyShoot), Alignment.Vertical);

            bool result = ship.GotHit(enemyShoot);

            Assert.True(result);
        }

        [Fact]
        public void WhenShootingToShipAndAllShipSegmentsWereHitThenShipShouldBeDestroyed()
        {
            var ship = new Ship(5, new Coordinates("C2"), Alignment.Vertical);
            ship.GotHit("C2");
            ship.GotHit("C3");
            ship.GotHit("C4");
            ship.GotHit("C5");
            ship.GotHit("C6");

            bool result = ship.IsDestroyed();

            Assert.True(result);
        }

        [Fact]
        public void WhenShootingToShipAndNotDestroyingItCompletelyThenShipShouldNotBeDestroyed()
        {
            var ship = new Ship(5, new Coordinates("C2"), Alignment.Vertical);
            ship.GotHit("C2");
            ship.GotHit("D2");
            ship.GotHit("F2");
            ship.GotHit("G2");

            bool result = ship.IsDestroyed();

            Assert.False(result);
        }

        [Fact]
        public void HittingSeveralTimesTheSameShipSegemntShouldNotDestroyShip()
        {
            var ship = new Ship(5, new Coordinates("C2"), Alignment.Vertical);
            ship.GotHit("C2");
            ship.GotHit("C2");
            ship.GotHit("C2");
            ship.GotHit("C2");
            ship.GotHit("C2");

            bool result = ship.IsDestroyed();

            Assert.False(result);
        }

        [Fact]
        public void EachSegemntOfShipCanBeReportedAsDestroyedOnlyOnce()
        {
            var ship = new Ship(5, new Coordinates("C2"), Alignment.Vertical);

            for (int i = 0; i < 2; i++)
            {
                ship.GotHit("C2");
                ship.GotHit("C3");
                ship.GotHit("C4");
                ship.GotHit("C5");
                ship.GotHit("C6");
            }

            bool result = ship.IsDestroyed();

            Assert.True(result);
        }
    }
}
