﻿using BattleShipGameLogic;
using System;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class ParsingPositionToCoordinatesTests
    {

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("    ")]
        public void PositionCannotBeNullEmptyOrBuildOnlyWithWhiteSpaceCharacters(string position)
        {
            var exception = Assert.Throws<ArgumentException>(() => new Coordinates(position));
            Assert.Equal("Position cannot be null, empty or contain only white-space characters.", exception.Message);
        }

        [Theory]
        [InlineData("1A")]
        [InlineData("10B")]
        [InlineData("1")]
        public void ColumnShouldBeGivenFirstrAndBeLetter(string position)
        {
            var exception = Assert.Throws<ArgumentException>(() => new Coordinates(position));
            Assert.Equal("Column is not a letter.", exception.Message);
        }

        [Theory]
        [InlineData("Z  ")]
        [InlineData("Z")]
        [InlineData("Z\t")]
        [InlineData("Z\n")]
        public void RowShouldBeGivenAfterColumnAndBeNumber(string position)
        {
            var exception = Assert.Throws<ArgumentException>(() => new Coordinates(position));
            Assert.Equal("Cannot properly parse row number.", exception.Message);
        }

        [Theory]
        [InlineData("A1B")]
        [InlineData("A 1B ")]
        [InlineData("A 1 B ")]
        [InlineData("ZZ")]
        public void RowShouldBeGivenAfterColumnAndBeANumber(string position)
        {
            var exception = Assert.Throws<ArgumentException>(() => new Coordinates(position));
            Assert.Equal("Cannot properly parse row number.", exception.Message);
        }

        [Theory]
        [InlineData("A1", 'A', 1)]
        [InlineData("A-1", 'A', -1)]
        [InlineData("Z10", 'Z', 10)]
        [InlineData("Z-999", 'Z', -999)]
        [InlineData("Ź11", 'Ź', 11)]
        [InlineData("A 1", 'A', 1)]
        [InlineData(" A1", 'A', 1)]
        [InlineData("     A1", 'A', 1)]
        [InlineData("\tA1", 'A', 1)]
        [InlineData("\t\t\tA1", 'A', 1)]
        [InlineData("\n\t\n\t  A1", 'A', 1)]
        public void PositionShouldBeParsedForProperCharAndNumber(string position, char expectedColumn, int expectedRow)
        {
            var coordinate = new Coordinates(position);

            Assert.Equal(new Coordinates(expectedColumn, expectedRow), coordinate);
        }


        [Theory]
        [InlineData("a1", 'A')]
        [InlineData(" z1", 'Z')]
        [InlineData("a\n1", 'A')]
        [InlineData("a\t1", 'A')]
        [InlineData("a\t\t\t\t1", 'A')]
        [InlineData("a          1", 'A')]
        public void ColumnShouldBaCaseInsensitive(string position, char expectedColumn)
        {
            var coordinate = new Coordinates(position);

            Assert.Equal(new Coordinates(expectedColumn, 1), coordinate);
        }

    }
}
