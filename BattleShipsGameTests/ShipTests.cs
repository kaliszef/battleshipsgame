﻿using BattleShipGameLogic;
using System;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class ShipTests
    {

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(int.MinValue)]
        public void ShipSizeCannotBeNegativeOrZero(int shipSize)
        {
            var shipSizeException = Assert.Throws<Exception>(() => new Ship(shipSize, new Coordinates('A', 1), Alignment.Vertical));
            Assert.Equal("Ships with no (zero) size or negative size cannot exist.", shipSizeException.Message);
        }

        [Theory]
        [InlineData(5, 'A', 1, Alignment.Vertical)]
        [InlineData(5, 'Z', 1, Alignment.Vertical)]
        [InlineData(5, 'a', 1, Alignment.Vertical)]
        [InlineData(4, 'z', 1, Alignment.Vertical)]
        public void CreatedShipHirozontallyShouldHaveProperCoordinatesOnOcean(int shipSize, char shipStartColumnPosition, int shipStartRowPosition, Alignment shipAlignment)
        {
            Coordinates starPositios = new Coordinates(shipStartColumnPosition, shipStartRowPosition);
            var ship = new Ship(shipSize, starPositios, shipAlignment);
 
            Assert.NotEmpty(ship.Coordinates);
            Assert.Equal(shipSize, ship.Coordinates.Length);
            Assert.Equal(starPositios, ship.Coordinates[0]);
            
            Coordinates endPosition = new Coordinates(shipStartColumnPosition, shipSize);
            Assert.Equal(endPosition, ship.Coordinates[shipSize-1]);
        }

        [Theory]
        [InlineData(5, 'B', 2, Alignment.Horizontal)]
        [InlineData(5, 'Z', 2, Alignment.Horizontal)]
        [InlineData(5, 'z', 2, Alignment.Horizontal)]
        public void CreatedShipVerticallyShouldHaveProperCoordinatesOnOcean(int shipSize, char shipStartColumnPosition, int shipStartRowPosition, Alignment shipAlignment)
        {
            Coordinates starPositios = new Coordinates(shipStartColumnPosition, shipStartRowPosition);
            var ship = new Ship(shipSize, starPositios, shipAlignment);

            Assert.NotEmpty(ship.Coordinates);
            Assert.Equal(shipSize, ship.Coordinates.Length);
            Assert.Equal(starPositios, ship.Coordinates[0]);

            char endColumPositionn = (Char)(Convert.ToUInt16(Char.ToUpper(shipStartColumnPosition)) + shipSize - 1);
            Coordinates endPositios = new Coordinates(endColumPositionn, shipStartRowPosition);
            Assert.Equal(endPositios, ship.Coordinates[shipSize - 1]);
        }

        [Fact]
        public void WhenComparingShipWithOtherObjectTypeComparisionShouldFail()
        {
            Ship ship = new Ship(2, new Coordinates("A1"), Alignment.Horizontal);
            Board board = new Board();

            var result = ship.Equals(board);

            Assert.False(result);
        }
    }
}