﻿using BattleShipGameLogic;
using System;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class GameBulderTests
    {

        [Fact]
        public void WhenCreatedBoardIsSmallerThenAnyShipThrowException()
        {
            var exception = Assert.Throws<ArgumentException>(() => BoardBuilder.CreateBoardWithShips(boardColumns: 3, boardRows: 3));
            Assert.Equal("Cannot add ship larger than Board Size.", exception.Message);
        }

        [Fact]
        public void EachSuccesfullyCreatedDefaultGameShouldHaveOneBoardAndAndThreeShips()
        {
            var board = BoardBuilder.CreateBoardWithShips();

            Assert.NotNull(board);
            int expectedShisCount = 3;
            Assert.Equal(expectedShisCount, board.ShipsCount);
        }
    }
}
