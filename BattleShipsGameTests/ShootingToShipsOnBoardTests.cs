﻿using BattleShipGameLogic;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class ShootingToShipsOnBoardTests
    {
        private Board board;

        public ShootingToShipsOnBoardTests()
        {
            board = new Board();
            board.AddShip(new Ship(5, new Coordinates("A1"), Alignment.Horizontal));
        }

        [Fact]
        public void WhenShootingAndNotHittingAnyShipOnBoardThenReportMiss()
        {
            var result = board.Shoot("A2");

            Assert.Equal(ShotReport.Miss, result);
        }

        [Fact]
        public void WhenShootingAndHittingAnyShipOnBoardThenReportHit()
        {
            var result = board.Shoot("E1");

            Assert.Equal(ShotReport.Hit, result);
        }

        [Fact]
        public void WhenShootingAndHittingLastNotDestroyedShipSegmentOfAnyShipOnBoardThenReportSinks()
        {
            board.Shoot("A1");
            board.Shoot("B1");
            board.Shoot("C1");
            board.Shoot("D1");
            var result = board.Shoot("E1");

            Assert.Equal(ShotReport.Sinks, result);
        }

        [Fact]
        public void EachShipOnBoardCanBeReportedAsDestroyedOnlyOnce()
        {

            for (int i = 0; i < 2; i++)
            {
                board.Shoot("A1");
                board.Shoot("B1");
                board.Shoot("C1");
                board.Shoot("D1");
                board.Shoot("E1");
            }

            Assert.True(board.AllShipsDestroyed());
        }
    }
}
