using BattleShipGameLogic;
using System;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class AddingShipsToBoardTests
    {

        [Fact]
        public void AddedShipCannotHaveAnyCommonCoordinatesWithOtherShipsLocatedOnBoard()
        {
            var board = new Board();
            board.AddShip(new Ship(4, new Coordinates("A1"), Alignment.Vertical));

            var exception = Assert.Throws<ShipsCollisionException>(() => board.AddShip(new Ship(4, new Coordinates("A1"), Alignment.Horizontal)));

            Assert.Equal("Cannot add ship coliding with any other ship on board.", exception.Message);
        }

        [Theory]
        [InlineData(4, "A11", Alignment.Vertical)]
        [InlineData(5, "A-1", Alignment.Horizontal)]
        [InlineData(5, "G10", Alignment.Horizontal)]
        public void CannotAddToBoardShipWhichAnySegmentStickOutBeyoundBoardBondaries(int shipSize, string shipHeadPosition, Alignment shipAlignment)
        {
            var board = new Board();

            var exception = Assert.Throws<ShipOutsideBoardException>(() => board.AddShip(new Ship(shipSize, new Coordinates(shipHeadPosition), shipAlignment)));

            Assert.Equal("Cannot add ship located outside board.", exception.Message);
        }

        [Theory]
        [InlineData(4, "A6", Alignment.Vertical)]
        [InlineData(5, "E10", Alignment.Horizontal)]
        public void WhenAddingShipToBoardAndAllShipSegmentsAreWithinBoardBoundariesShipShouldBeAddedToBoard(int shipSize, string shipHeadPosition, Alignment shipAlignment)
        {
            var board = new Board();

            board.AddShip(new Ship(shipSize, new Coordinates(shipHeadPosition), shipAlignment));

            Assert.Equal(1, board.ShipsCount);
        }

        [Theory]
        [InlineData(2, 2, 3, Alignment.Horizontal)]
        [InlineData(2, 1, 2, Alignment.Vertical)]
        [InlineData(1, 2, 2, Alignment.Horizontal)]
        public void CannotAddShipLargerThanBoardSize(int columnsCount, int rowsCount, int shipSize, Alignment shipAlignment)
        {
            var board = new Board(columnsCount, rowsCount);

            var exception = Assert.Throws<ArgumentException>(() => board.AddShip(new Ship(shipSize, new Coordinates("A1"), shipAlignment)));

            Assert.Equal("Cannot add ship larger than Board Size.", exception.Message);
        }

        [Fact]
        public void TotalShipsCapacityAddedToBoardCannotBeGreaterThanBoardCapacity()
        {
            var board = new Board(2, 2);

            board.AddShip(new Ship(2, new Coordinates("A1"), Alignment.Vertical));
            board.AddShip(new Ship(2, new Coordinates("B1"), Alignment.Vertical));

            var exception = Assert.Throws<ArgumentException>(() => board.AddShip(new Ship(2, new Coordinates("C1"), Alignment.Horizontal)));

            Assert.Equal("Cannot add more ships to Board than Board capcity can handle.", exception.Message);
        }

        [Fact]
        public void WhenAddingShipsWithTotalCapacityLowerThanBoardCapacityThenAllShipsShouldBeAddedToBoard()
        {
            var board = new Board(2, 2);

            board.AddShip(new Ship(2, new Coordinates("A1"), Alignment.Vertical));
            board.AddShip(new Ship(2, new Coordinates("B1"), Alignment.Vertical));

            var expectedShipsCount = 2;
            Assert.Equal(expectedShipsCount, board.ShipsCount);
        }
    }
}
