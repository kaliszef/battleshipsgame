﻿using BattleShipGameLogic;
using System.Collections.Generic;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class GettigDestroyedShipsCoordinatesTests
    {
        private readonly Board board;

        public GettigDestroyedShipsCoordinatesTests()
        {
            board = new Board(3, 3);
            board.AddShip(new Ship(2, new Coordinates("A1"), Alignment.Horizontal));
            board.Shoot("A1");
            board.Shoot("B1");
        }

        [Theory]
        [InlineData("A1")]
        [InlineData("B1")]
        public void WhenAnyOfDestroyedShipSegmentIsTargetedThenReturnAllItsSegmentsCoordniates(string target)
        {
            var actualSegments = board.GetDestroyedShipSegments(target);
            var expecedSegments = new List<Coordinates>() {
                new Coordinates("A1"),
                new Coordinates("B1")
            };

            Assert.Equal(expecedSegments, actualSegments);
        }

        [Theory]
        [InlineData("C1")]
        [InlineData("A2")]
        [InlineData("B2")]
        [InlineData("C2")]
        [InlineData("A3")]
        [InlineData("B3")]
        [InlineData("C3")]
        public void WhenNoneOfDestroyedShipsSegmentIsTargetedThenReturnEmptyCoordinatesCollection(string target)
        {
            var actualSegments = board.GetDestroyedShipSegments(target);
            Assert.Empty(actualSegments);
        }
    }
}
