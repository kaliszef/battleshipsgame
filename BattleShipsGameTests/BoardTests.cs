﻿using BattleShipGameLogic;
using System;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class BoardTests
    {
        [Fact]
        public void CreatingBoardWithDefaultConstructorShouldCreate10x10Board()
        {
            Board board = new Board();

            Assert.NotNull(board);
            Assert.Equal(10, board.ColumnsCount);
            Assert.Equal(10, board.RowsCount);
            Assert.Equal(0, board.ShipsCount);
        }

        [Theory]
        [InlineData(1, 1)]
        [InlineData(2, 10)]
        [InlineData(10, 10)]
        public void CreatingCustomSizedBoardWithPositiveColsAndRowsNumbersShouldEndWithSuccess(int columns, int rows)
        {
            Board board = new Board(columns, rows);

            Assert.NotNull(board);
            Assert.Equal(columns, board.ColumnsCount);
            Assert.Equal(rows, board.RowsCount);
            Assert.Equal(0, board.ShipsCount);
        }

        [Theory]
        [InlineData(int.MaxValue, int.MaxValue)]
        [InlineData(short.MaxValue, int.MaxValue)]
        [InlineData(int.MaxValue, short.MaxValue)]
        public void WhenCreatingTooBigBoardExceptionShouldBeThrown(int rows, int columns)
        {
            var exception = Assert.Throws<BoarSizeException>(() => new Board(columns, rows));
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(-10, -10)]
        [InlineData(-1, 10)]
        [InlineData(10, -1)]
        [InlineData(int.MinValue, int.MinValue)]
        public void WhenCreatingBoardWithNegativeOrZeroValueParametersExceptionShouldBeThrown(int rows, int columns)
        {
            var exception = Assert.Throws<ShipOutsideBoardException>(() => new Board(columns, rows));

            Assert.Equal("Cannot create zero sized or negative sized board for BattleShips game.", exception.Message);
        }

        [Fact]
        public void WhenRandomizingColumnItShouldBeWithinBoardBoundaries()
        {
            Board board = new Board(3, 3);
            for (int i = 0; i < 1000; i++)
            {
                char randomColumnLetter = board.RandomizeColumn();
                Assert.True(board.ColumnsRange.Contains(randomColumnLetter));
            }
        }

        [Fact]
        public void WhenRandomizingRowItShouldBeWithinBoardBoundaries()
        {
            Board board = new Board(3, 3);
            for (int i = 0; i < 1000; i++)
            {
                int randomRow = board.RandomizeRow();
                Assert.True(board.RowsRange.Contains(randomRow));
            }
        }

        [Theory]
        [InlineData('A', 0)]
        [InlineData('B', 1)]
        [InlineData('C', 2)]
        public void WhenTryingToFindColumnValueAndItExistInBoardThenReturnItsIndex(char columnValue, int expectedIndex)
        {
            Board board = new Board(3, 3);

            var actualIndex = board.GetColumnIndex(columnValue);

            Assert.Equal(expectedIndex, actualIndex);
        }

        [Fact]
        public void WhenTryingToFindColumnValueAndItDoesNotExistInBoardThenThrowException()
        {
            Board board = new Board(3, 3);
            Assert.Throws<ArgumentException>(() => board.GetColumnIndex('D'));
        }

        [Theory]
        [InlineData(1, 0)]
        [InlineData(2, 1)]
        [InlineData(3, 2)]
        public void WhenTryingToFindRowValueAndItExistInBoardThenReturnItsIndex(int rowValue, int expectedIndex)
        {
            Board board = new Board(3, 3);

            var actualIndex = board.GetRowIndex(rowValue);

            Assert.Equal(expectedIndex, actualIndex);
        }

        [Fact]
        public void WhenTryingToFindRowValueAndItDoesNotExistInBoardThenThrowException()
        {
            Board board = new Board(3, 3);
            Assert.Throws<ArgumentException>(() => board.GetRowIndex(4));
        }

    }
}
