﻿using BattleShipsGame;
using Xunit;

namespace BattleShipsGameLogicTests
{
    public class MatrixExtensionsTests
    {
        private char[,] matrix;

        public MatrixExtensionsTests()
        {
            matrix = new char[,] {
                { 'A', 'B', 'C', 'D'},
                { 'E', 'F', 'G', 'H'},
                { 'I', 'J', 'K', 'L'}
            };
        }

        [Fact]
        public void RowsCountShouldReturnProperNumberOfRowsFromTwoDimentionalMatrix()
        {
            int expectedRows = 3;
            Assert.Equal(expectedRows, matrix.RowsCount());
        }

        [Fact]
        public void ColumnsCountShouldReturnProperNumberOfColumnsFromTwoDimentionalMatrix()
        {
            int expectedColumns = 4;
            Assert.Equal(expectedColumns, matrix.ColumnsCount());
        }

        [Fact]
        public void GetRowShouldReturnAllItemsFromGivenRow()
        {
            char[] actual = matrix.GetRow(2);

            Assert.Equal(4, actual.Length);

            var expected = new[] { 'I', 'J', 'K', 'L' };
            Assert.Equal(expected, actual);
        }
    }
}
