1. Download .net core 3.1 Build apps - SDK i.e. from : 'https://dotnet.microsoft.com/download/dotnet/3.1'
2. Clone git repository using command: 'git clone https://kaliszef@bitbucket.org/kaliszef/battleshipsgame.git' to your local machine.
3. Go to 'BattleShip.sln' solution file location in cloned git repository using cmd shell (on Windows) (or other shell build in your OS).
4. Type command 'dotnet build --configuration Release' to build game.
5. Go to ...BattleShipsGame/BattleShipsGame folder where 'BattleShipsGame.csproj' file is located.
6. Run application using command 'dotnet run --configuration Release --'