﻿using System;

namespace BattleShipGameLogic
{
    [Serializable]
    public class ShipsCollisionException : Exception
    {
        public ShipsCollisionException(string message) : base(message)
        {
        }
    }
}