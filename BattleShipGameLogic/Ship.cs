﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleShipGameLogic
{
    public enum Alignment
    {
        Vertical,
        Horizontal
    }

    public class Ship
    {
        private readonly int size;
        private readonly Alignment alignment;
        private HashSet<Coordinates> hitReport = new HashSet<Coordinates>();

        public Coordinates[] Coordinates { get; private set; }
        public int Size => size;

        public Ship(int shipSize, Coordinates StartCoordinates, Alignment alignment)
        {
            if (shipSize <= 0)
            {
                throw new Exception("Ships with no (zero) size or negative size cannot exist.");
            }

            size = shipSize;
            this.alignment = alignment;
            Coordinates = new Coordinates[shipSize];
            Coordinates[0] = new Coordinates(StartCoordinates.Column, StartCoordinates.Row);

            PutShipOnBoard();
        }

        public bool GotHit(string enemyShootCoordinates)
        {
            var enemyShoot = new Coordinates(enemyShootCoordinates);

            if(Coordinates.Contains(enemyShoot))
            {
                hitReport.Add(enemyShoot);
                return true;
            }

            return false;
        }

        public bool ColideWith(IEnumerable<Ship> shipsOnBoard)
        {
            foreach (var ship in shipsOnBoard)
            {
                bool isShipsColision = Coordinates.Where(singlePosion => ship.Coordinates.Contains(singlePosion)).Any();
                if (isShipsColision)
                    return true;
            }
            return false;
        }

        public bool ExceedesBoardSize(int boardColumnsCount, int boardRowsCount)
        {
            if (alignment == Alignment.Horizontal)
                return Coordinates.Length > boardColumnsCount;

            return Coordinates.Length > boardRowsCount;
        }

        public bool IsOutsideBoard(IList<char> boardColumnsRange, IList<int> boardRowsRange)
        {
            foreach (var shipSquare in Coordinates)
            {
                var isShipSquareBeyondBoardColumnsRange = !boardColumnsRange.Contains(shipSquare.Column);
                var isShipSquareBeyoundBoardRowsRange = !boardRowsRange.Contains(shipSquare.Row);

                if (isShipSquareBeyondBoardColumnsRange || isShipSquareBeyoundBoardRowsRange)
                    return true;
            }
            return false;
        }

        public bool IsDestroyed()
        {
            return hitReport.Count == Size;
        }

        public override bool Equals(object other)
        {
            if (other == null || !(other is Ship))
                return false;
            Ship otherCasted = (Ship)other;

            return Size == otherCasted.Size && Coordinates.SequenceEqual(otherCasted.Coordinates);
        }

        public override int GetHashCode()
        {
            var hashSum = size;
            foreach (var segment in Coordinates)
            {
                hashSum = hashSum + segment.Row + segment.Column;
            }

            return hashSum.GetHashCode();
        }

        private void PutShipOnBoard()
        {
            switch(alignment)
            {
                case Alignment.Vertical: TopDown();
                    break;
                case Alignment.Horizontal: LeftToRight();
                    break;
            }
        }

        private void LeftToRight()
        {
            for (int i = 1; i < Coordinates.Length; i++)
            {
                char column = (char)(Convert.ToUInt16(Coordinates[i - 1].Column) + 1);
                int row = Coordinates[i - 1].Row;
                Coordinates[i] = new Coordinates(column, row);
            }
        }

        private void TopDown()
        {
            for (int i = 1; i < Coordinates.Length; i++)
            {
                char column = Coordinates[i - 1].Column;
                int row = Coordinates[i - 1].Row + 1;
                Coordinates[i] = new Coordinates(column, row);
            }
        }
    }
}