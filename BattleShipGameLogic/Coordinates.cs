﻿using System;

namespace BattleShipGameLogic
{
    public class Coordinates
    {

        private char colum;
        public char Column
        {
            get { return char.ToUpper(colum); }
            private set { colum = value; }
        }
        public int Row { get; private set; }

        public Coordinates(char column, int row)
        {
            Column = column;
            Row = row;
        }

        public Coordinates(string position)
        {
            var coordinates = ParseCoordinates(position);

            Column = coordinates.Column;
            Row = coordinates.Row;
        }

        public override bool Equals(object other)
        {
            if (other == null || !(other is Coordinates))
                return false;
            Coordinates otherCasted = (Coordinates)other;

            return otherCasted.Column == Column && otherCasted.Row == Row;
        }

        public override int GetHashCode()
        {
            var hashSum = Row + Column;
            return hashSum.GetHashCode();
        }

        private Coordinates ParseCoordinates(string position)
        {
            if (string.IsNullOrWhiteSpace(position))
            {
                throw new ArgumentException("Position cannot be null, empty or contain only white-space characters.");
            }

            var trimmedPosiotion = position.Trim();
            char column = GetColumn(trimmedPosiotion);
            int row = GetRow(trimmedPosiotion);

            return new Coordinates(column, row);

        }

        private static char GetColumn(string position)
        {
            var column = position[0];
            if (!char.IsLetter(column))
            {
                throw new ArgumentException("Column is not a letter.");
            }

            return column;
        }

        private static int GetRow(string position)
        {
            var rowStr = position.Substring(1);
            var rowStrTrimmed = rowStr.Trim();

            bool rowParsedSucessfully = int.TryParse(rowStrTrimmed, out var row);
            if (!rowParsedSucessfully)
            {
                throw new ArgumentException("Cannot properly parse row number.");
            }

            return row;
        }

    }
}