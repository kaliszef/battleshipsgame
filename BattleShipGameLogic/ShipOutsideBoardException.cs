﻿using System;

namespace BattleShipGameLogic
{
    public class ShipOutsideBoardException : Exception
    {
        public ShipOutsideBoardException(string message) : base(message) 
        {
        }
    }
}