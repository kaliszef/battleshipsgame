﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleShipGameLogic
{
    public enum ShotReport
    {
        Miss,
        Hit,
        Sinks
    }

    public class Board
    {
        private const int columnOrigin = 'A';
        private const int rowsOrigin = 1;

        private readonly IList<Ship> ships = new List<Ship>();
        private readonly HashSet<Ship> destroyedShips = new HashSet<Ship>();

        public IList<char> ColumnsRange { get; private set; }
        public IList<int> RowsRange { get; private set; }

        public int ColumnsCount => ColumnsRange.Count;
        public int RowsCount => RowsRange.Count;
        public int ShipsCount => ships.Count;

        public Board(int columnsCount = 10, int rowsCount = 10)
        {
            if (columnsCount <= 0 || rowsCount <= 0)
                throw new ShipOutsideBoardException("Cannot create zero sized or negative sized board for BattleShips game.");

            try
            {
                ColumnsRange = Enumerable.Range(columnOrigin, columnsCount).Select(letter => (char)letter).ToList();
                RowsRange = Enumerable.Range(rowsOrigin, rowsCount).ToList();
            }
            catch
            {
                throw new BoarSizeException($"Cannot Create such a big board: {columnsCount} x {rowsCount}. Try With smaller values.");
            }
        }

        public void AddShip(Ship newShip)
        {
            if (NotEnoughSpaceForAnother(newShip))
                throw new ArgumentException("Cannot add more ships to Board than Board capcity can handle.");

            if (newShip.ExceedesBoardSize(ColumnsCount, RowsCount))
                throw new ArgumentException("Cannot add ship larger than Board Size.");

            if (newShip.IsOutsideBoard(ColumnsRange, RowsRange))
                throw new ShipOutsideBoardException("Cannot add ship located outside board.");

            if (newShip.ColideWith(ships))
                throw new ShipsCollisionException("Cannot add ship coliding with any other ship on board.");

            ships.Add(newShip);
        }

        public ShotReport Shoot(string target)
        {
            foreach (var ship in ships)
            {
                if (ship.GotHit(target))
                {
                    if (ship.IsDestroyed())
                    {
                        destroyedShips.Add(ship);
                        return ShotReport.Sinks;
                    }
                    return ShotReport.Hit;
                }
            }

            return ShotReport.Miss;
        }

        public bool AllShipsDestroyed()
        {
            return destroyedShips.Count == ships.Count;
        }

        public IEnumerable<Coordinates> GetDestroyedShipSegments(string target)
        {
            var tarrgetCoordinates = new Coordinates(target);
            foreach (var destroyedShip in destroyedShips)
            {
                if (destroyedShip.Coordinates.Contains(tarrgetCoordinates))
                {
                    return destroyedShip.Coordinates;
                }
            }
            return new List<Coordinates>();
        }

        public int GetColumnIndex(char columnValue)
        {
            var foundIndex = ColumnsRange.IndexOf(columnValue);
            if (foundIndex == -1)
                throw new ArgumentException($"There is not such column in board: {columnValue}");

            return foundIndex;
        }

        public int GetRowIndex(int rowValue)
        {
            var foundIndex = RowsRange.IndexOf(rowValue);
            if (foundIndex == -1)
                throw new ArgumentException($"There is not such row in board: {rowValue}");

            return foundIndex;
        }

        public int RandomizeRow()
        {
            Random r = new Random();
            return r.Next(rowsOrigin, RowsCount + 1);
        }

        public char RandomizeColumn()
        {
            Random r = new Random();
            var randomColumnPosition = r.Next(0, RowsCount);

            return ColumnsRange[randomColumnPosition];
        }

        private bool NotEnoughSpaceForAnother(Ship newShip)
        {
            var boardCapacity = ColumnsCount * RowsCount;
            var shipsTotalCapacity = ships.Aggregate(0, (total, nextShip) => total + nextShip.Size);

            if (shipsTotalCapacity + newShip.Size > boardCapacity)
                return true;
            return false;
        }
    }
}