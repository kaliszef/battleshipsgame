﻿using System;

namespace BattleShipGameLogic
{
    [Serializable]
    public class BoarSizeException : Exception
    {
        public BoarSizeException(string message) : base(message)
        {
        }
    }
}