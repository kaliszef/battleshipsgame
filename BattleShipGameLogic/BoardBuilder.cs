﻿using System;

namespace BattleShipGameLogic
{
    public static class BoardBuilder
    {
        public static Board CreateBoardWithShips(int boardColumns = 10, int boardRows = 10, int numberOfDestroyers = 2, int numberOfBattleships = 1)
        {
            var computerBoard = CreateBoard(boardColumns, boardRows);
            AddRandomizedBattleships(numberOfBattleships, computerBoard);
            AddRandomizedDestroyers(numberOfDestroyers, computerBoard);

            return computerBoard;
        }

        private static Board CreateBoard(int boardColumns, int boardRows)
        {
            return new Board(boardColumns, boardRows);
        }

        private static void AddRandomizedDestroyers(int numberOfDestroyers, Board board)
        {
            int destroyerSize = 4;
            AddRandomizedShipToBoard(numberOfDestroyers, destroyerSize, board);
        }

        private static void AddRandomizedBattleships(int numberOfBattleships, Board board)
        {
            int battleshipSize = 5;
            AddRandomizedShipToBoard(numberOfBattleships, battleshipSize, board);
        }

        private static void AddRandomizedShipToBoard(int numberOfShips, int shipSize, Board board)
        {
            int createdShips = 0;

            while (createdShips < numberOfShips)
            {
                try
                {
                    Ship newship = RandomizeShipPositionOnBoard(shipSize, board);
                    board.AddShip(newship);

                    createdShips++;
                }
                catch (Exception ex) 
                {
                    if(ex is ShipOutsideBoardException || ex is ShipsCollisionException)
                        continue;
                    throw;
                }
            }

        }

        private static Ship RandomizeShipPositionOnBoard(int shipSize, Board board)
        {
            var startColum = board.RandomizeColumn();
            var startRow = board.RandomizeRow();
            var alignment = RandomizeShipAlignment();
            var shipHeadCoordinates = new Coordinates(startColum, startRow);

            var newship = new Ship(shipSize, shipHeadCoordinates, alignment);
            return newship;
        }

        private static Alignment RandomizeShipAlignment()
        {
            Random rand = new Random();
            var alignementItemsCount = Enum.GetNames(typeof(Alignment)).Length;
            var randomizedAlignemt = rand.Next(0, alignementItemsCount);

            return (Alignment)randomizedAlignemt;
        }
    }
}